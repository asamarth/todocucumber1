Feature: the Todo can be retrieved
  Scenario: client makes call to GET /todos
   	Given todo rest endpoint of "35.223.78.207:9010" is up
    When the client calls /todos
    Then the client receives status code of 200